document.addEventListener('DOMContentLoaded', init, false);
function init() {
    var order = [5,4,3,2,1], j = 1;
    order.forEach(i => {
        var button = document.createElement('button');
        button.innerText = i;
        button.setAttribute('position', j);
        document.querySelector('div.buttons').appendChild(button);
        button.addEventListener('click', changeOrder, false );
        j++;
    });
    document.querySelectorAll(".souvenir").forEach(suv => suv.style.background = "rgb(" + Math.floor(Math.random() * 20) + 1 + "," + Math.floor(Math.random() * 20) + 1 + "," + Math.floor(Math.random() * 20) + 1 + ")");
}
 
function changeOrder(divButtons) {
    var buttons = document.getElementsByTagName('button');
    var divButtons = document.querySelector('div.buttons');
    this.parentNode.removeChild(this);
    if (this.getAttribute('position') != this.innerText) {
        var nextButton = buttons[parseInt(this.innerText)-1];
        divButtons.insertBefore(this, nextButton);       
    } else {
        divButtons.insertBefore(this, divButtons.firstChild);
    }
    for(var k = 0; k < buttons.length; k++) {
        buttons[k].setAttribute('position', k + 1);
    }
}